const createCard = async (card) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/create-cards`, {
            method: "POST",
            body: card,
        });
        if (response?.data?.success === false) {
            const errorMessage = response?.data?.message || "Erro ao criar o card";
            throw new Error(errorMessage);
        }
        return response.data.value;
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};
const moveCard = async (moveCard) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/move-cards`, {
            method: "POST",
            body: moveCard,
        });
        if (response?.data?.success === false) {
            const errorMessage = response?.data?.message || "Erro ao criar o card";
            throw new Error(errorMessage);
        }
        return response.data.value;
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};

const deleteCard = async (card_id) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/delete-cards`, {
            method: "POST",
            body: {
                card_id: card_id
            } 
        });
        if (response?.data?.success === false) {
            const errorMessage = response?.data?.message || "Erro ao criar o card";
            throw new Error(errorMessage);
        }
        return response.data.value;
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};
export default {
    createCard,
    moveCard,
    deleteCard
};
