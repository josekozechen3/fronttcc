const createProject = async (project) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/create-project`, {
            method: "POST",
            body: project,
        });
        if (response?.data?.success === false) {
            const errorMessage = response?.data?.message || "Erro ao criar o card";
            throw new Error(errorMessage);
        }
        return response.data.value;
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};

const deleteProject = async (project) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/delete-project`, {
            method: "POST",
            body: {
                project_id: project
            } 
        });
        if (response?.data?.success === false) {
            const errorMessage = response?.data?.message || "Erro ao criar o card";
            throw new Error(errorMessage);
        }
        return response.data.value;
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};

const getProjects = async (user_id = 0) => {
    try {
        const body = {};
        const response = await useFetch(`http://localhost:8001/api/list-project`, {
            method: "POST",
            body: {
                user_id: user_id,
            },
        });
        console.log("response2", response);
        if (response?.data?.value?.success) {
            let faqsResponse = response.data.value;
            return faqsResponse;
        } else {
            throw new Error("Falha ao buscar Faqs!");
        }
    } catch (error) {
        throw new Error(error.message);
    }
};
const getProgess = async (project_id = 0) => {
    try {
        const body = {};
        const response = await useFetch(`http://localhost:8001/api/list-project/porcentagem`, {
            method: "POST",
            body: {
                project_id: project_id,
            },
        });
        if (response?.data?.value?.success) {
            let faqsResponse = response.data.value;
            return faqsResponse;
        } else {
            throw new Error("Falha ao buscar Faqs!");
        }
    } catch (error) {
        throw new Error(error.message);
    }
};
const getProjectsDetails = async (project_id = 0) => {
    try {
        const body = {};
        const response = await useFetch(`http://localhost:8001/api/details-project`, {
            method: "POST",
            body: {
                project_id: project_id,
            },
        });
        console.log("response2", response);
        if (response?.data?.value?.success) {
            let faqsResponse = response.data.value;
            return faqsResponse;
        } else {
            throw new Error("Falha ao buscar Faqs!");
        }
    } catch (error) {
        throw new Error(error.message);
    }
};

export default {
    getProjects,
    getProjectsDetails,
    createProject,
    deleteProject,
    getProgess
};