import { defineStore } from "pinia";
import projectService from "../utils/project.service";

export const useProjectStore = defineStore(
    "ProjectStore",
    () => {
        // state
        const projects = ref({});
        const project = ref({});

        // actions
        const $reset = (): void => {
            projects.value = {};
            project.value = {};
        };

        const createProject = async (data: any) => {
            try {
                const response = await projectService.createProject(data);
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };

        const getProjects = async (
            user_id: number,
        ) => {
            try {
                let projectList: any = await projectService.getProjects(
                    user_id,
                );     
                projects.value = projectList.data;
                let result = projectList.data;
                return result;
            } catch (error: any) {
                console.log("deu ruim");
                
            }
        };
        const getProgess = async (
            project_id: number,
        ) => {
            try {
                let projectList: any = await projectService.getProgess(
                    project_id,
                );     
                console.log("projectList.data", projectList);
                
                projects.value = projectList;
                let result = projectList;
                return result;
            } catch (error: any) {
                console.log("deu ruim");
            }
        };
        const getProjectsDetails = async (
            project_id: number,
        ) => {
            try {
                let projectList: any = await projectService.getProjectsDetails(
                    project_id,
                );     
                project.value = projectList.data;
                let result = projectList.data;
                return result;
            } catch (error: any) {
                console.log("deu ruim");
            }
        };

        const deleteCard = async (project_id: any) => {
            try {
                const response = await projectService.deleteProject(project_id);
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };

        return {
            project,
            projects,
            getProjects,
            getProjectsDetails,
            $reset,
            createProject,
            deleteCard,
            getProgess
        };
    },
    { persist: true }
);