import { defineStore } from "pinia";
import cardService from "../utils/card.service";

export const useCardStore = defineStore(
    "CardStore",
    () => {
        const card = ref({});
        const $reset = (): void => {
            card.value = [];
        };
        const createCard = async (data: any) => {
            try {
                const response = await cardService.createCard(data);
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };
        const moveCard = async (moveCard: any) => {
            try {
                const response = await cardService.moveCard(moveCard);
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };
        const deleteCard = async (card: any) => {
            try {
                const response = await cardService.deleteCard(card);
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };
        return {
            card,
            $reset,
            moveCard,
            createCard,
            deleteCard
        };
    },
    {
        persist: {
            storage: persistedState.localStorage,
        },
    }
);
