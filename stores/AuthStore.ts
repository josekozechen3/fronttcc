import { defineStore } from "pinia";
import authService from "../utils/auth.service";

export const useAuthStore = defineStore(
    "AuthStore",
    () => {
        // state
        const user = ref({
            email: '',
            password: '',
        });
        
        const $reset = (): void => {
            user.value = [];
        };

        const login = async (data: any) => {
            try {
                const response = await authService.login(data); 
                user.value = response.data;             
                let loginResult = response;               
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim");
                
            }
        };
        const register = async (data: any) => {
            try {
                const response = await authService.register(data);   
                let loginResult = response;
                return loginResult;
            } catch (error: any) {
                console.log("deu ruim:", error);
                throw new Error("Falha no registro: " + error.message);
            }
        };

        return {
            user,
            $reset,
            login,
            register
        };
    },
    {
        persist: {
            storage: persistedState.localStorage,
        },
    }
);
