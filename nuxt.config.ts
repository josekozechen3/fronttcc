import { env } from "process";

export default {
    ssr: false,
    runtimeConfig: {
    },
    components: [
        {
            path: "~/components",
            pathPrefix: false,
        },
    ],
    modules: [
        "@pinia/nuxt",
        "@pinia-plugin-persistedstate/nuxt",
        // "nuxt-sanctum-auth",
        "nuxt-lodash",
        '@invictus.codes/nuxt-vuetify',
    ],
    pinia: {
        autoImports: ["defineStore", "acceptHMRUpdate"],
    },
    pages: true,
    typescript: {
        shim: false,
    },
    build: {
        transpile: ["@vuepic/vue-datepicker"],
    },
    vuetify: {
        /* vuetify options */
        vuetifyOptions: {
          // @TODO: list all vuetify options
        },
    
        moduleOptions: {
          /* nuxt-vuetify module options */
          treeshaking: true,
          useIconCDN: true,
    
          /* vite-plugin-vuetify options */
          styles: true,
          autoImport: true,
          useVuetifyLabs: false, 
        }
      }
};
